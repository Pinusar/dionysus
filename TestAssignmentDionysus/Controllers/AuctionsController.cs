﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using TestAssignmentDionysus.Models;

namespace TestAssignmentDionysus.Controllers
{
    public class AuctionsController : Controller
    {
        public static string endtimeString(DateTime dt)
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
            string result = dt.ToString();
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("et-EE");
            return result;
        }

        private TestAssignment20190408111127_dbEntities db = new TestAssignment20190408111127_dbEntities();

        public static Auction getById(int id)
        {
            TestAssignment20190408111127_dbEntities db = new TestAssignment20190408111127_dbEntities();
            Auction auction = db.Auctions.Find(id);
            return auction;
        }

        // GET: Auctions
        public ActionResult Index()
        {
            foreach (var auction in db.Auctions)
            {
                var timeLeft = auction.EndTime - DateTime.Now;
                if (timeLeft <= new TimeSpan(0, 0, 0))
                {
                    auction.Status = 2;
                }
            }
            db.SaveChanges();
            return View(db.Auctions.ToList());
        }

        // GET: Auctions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Auction auction = db.Auctions.Find(id);
            if (auction == null)
            {
                return HttpNotFound();
            }
            return View(auction);
        }
    }
}
