﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TestAssignmentDionysus.Models;

namespace TestAssignmentDionysus.Controllers
{
    public class BidsController : Controller
    {
        private TestAssignment20190408111127_dbEntities db = new TestAssignment20190408111127_dbEntities();

        // GET: Bids/Create
        public ActionResult Create()
        {
            string urlParameter = RouteData.Values["id"] + Request.Url.Query;
            int aucId = int.Parse(urlParameter.Split('=')[1]);
            ViewBag.AucId = aucId;

            ViewBag.AuctionId = new SelectList(db.Auctions, "Id", "ProductName");
            return View();
        }

        // POST: Bids/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,AuctionId,User,Time,Sum")] Bid bid)
        {
            bid.Time = DateTime.Now;

            if (ModelState.IsValid)
            {
                db.Bids.Add(bid);
                db.SaveChanges();
                return RedirectToAction("Index", "Auctions");
            }

            ViewBag.AuctionId = new SelectList(db.Auctions, "Id", "ProductName", bid.AuctionId);
            return View(bid);
        }      

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
